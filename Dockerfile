FROM python:3.10-buster

COPY /src/setup.py /usr/src
WORKDIR /usr/src
RUN pip install .

COPY /src/broncobot /usr/src/app
WORKDIR /usr/src/app

ENTRYPOINT ["python3","./bot.py"]

