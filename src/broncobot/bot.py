# built-in imports
import time
# bot imports
import config
from utils.cogs_utils import BotCogs
# nextcord imports
import nextcord
from nextcord.ext import commands


def main():
    intents = nextcord.Intents.default()
    intents.guilds = True
    intents.members = True

    client = commands.Bot(command_prefix=config.PREFIX, intents=intents)

    # log Ready in the Bot Channel 

    @client.event
    async def on_ready():
        ts = int(round(time.time()))
        channel = client.get_channel(int(config.BRONCO_BOT_CHANNEL))
        await channel.send(f"Bronco_Bot On-line <t:{ts}:F>")

    # load all cogs
    bot_cogs = BotCogs()
    for cog in bot_cogs.all_cogs_full:
        cog_nick = bot_cogs.get_keys(cog)
        bot_cogs.loaded_cogs.append(cog_nick)
        client.load_extension(cog)

    # print loaded cogs
    @client.command()
    @commands.has_guild_permissions(administrator=True)
    async def print_cogs (ctx):
        if len(bot_cogs.loaded_cogs) > 0:
            await ctx.send("Loaded cogs:")
            for cog in bot_cogs.loaded_cogs:
                await ctx.send(f"{cog}")
            await ctx.send("No More Cogs Loaded")
        else:
            await ctx.send("No Cogs Loaded")

    # Manual load Cog
    @client.command()
    @commands.has_guild_permissions(administrator=True)
    async def load (ctx, cog):
        channel = client.get_channel(int(config.BRONCO_BOT_CHANNEL))
        if cog not in bot_cogs.loaded_cogs:
            bot_cogs.loaded_cogs.append(cog)
            client.load_extension(f'{bot_cogs.cog_dict.get(cog)}')
        else:
            await channel.send(f"{cog} already loaded")
        

    # Manual unload Cog
    @client.command()
    @commands.has_guild_permissions(administrator=True)
    async def unload (ctx, cog):
        channel = client.get_channel(int(config.BRONCO_BOT_CHANNEL))
        if cog in bot_cogs.loaded_cogs:
            bot_cogs.loaded_cogs.remove(cog)
            client.unload_extension(f'{bot_cogs.cog_dict.get(cog)}')
            await channel.send(f"{cog} unloaded")
        else:
            await channel.send(f"{cog} was not unloaded as it was not loaded")
    
    # Run the Bot
    client.run(config.BOT_TOKEN)


if __name__ == '__main__':
    main()
