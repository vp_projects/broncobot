import nextcord
from nextcord import Interaction, Embed, Color
from nextcord.ext import commands, tasks
import config
from .on_channel_view import OnChannelBot
from .off_channel_view import OffChannelBot



class ToggleChannelBot(commands.Cog, name="Channel Roles"):
    """Turns on and off Channels"""

    def __init__(self, client):
        self.client = client
        self.send_onready_message.start()
        
    #Send notification to log channel bot is loaded
    @tasks.loop(count=1)
    async def send_onready_message(self):
        channel = self.client.get_channel(int(config.BRONCO_BOT_CHANNEL))
        await channel.send('Channel Toggle loaded')

    # wait for the client before starting the task
    @send_onready_message.before_loop  
    async def before_send(self):
        await self.client.wait_until_ready()
        return

    @send_onready_message.after_loop
    async def before_send(self):
        self.send_onready_message.close()
        return
    
    @commands.Cog.listener()
    async def on_ready(self):
        """When the bot is ready, load the role view"""
        self.client.add_view(OnChannelBot())
        print("Button view added")

    @commands.command()
    @commands.is_owner()
    async def channel_on(self, ctx: commands.Context):
        """Starts a role view"""
        await ctx.send('Click a button to turn on a channel', view=OnChannelBot())
        
    @commands.command()
    @commands.is_owner()
    async def channel_off(self, ctx: commands.Context):
        """Starts a role view"""
        await ctx.send('Click a button to turn off a channel', view=OffChannelBot())
        
# setup functions for bot
def setup(client):
    client.add_cog(ToggleChannelBot(client))

'''
    @commands.Cog.listener()
    async def on_ready(self):
        """When the bot is ready, load the role view"""
        self.client.add_view(RoleView())
        print("Button view added")
'''