from utils.utils import custom_id, teams
import nextcord
from nextcord import Interaction
import config

import time



VIEW_NAME = "OnChannelBot"

class OnChannelBot(nextcord.ui.View):
    def __init__(self):
        super().__init__(timeout=None)
        
    async def on_click(self,
                            button: nextcord.ui.Button,
                            interaction: nextcord.Interaction):
    
        # get channel from the channel id
        channel = interaction.guild.get_channel(int(button.custom_id.split(":")[-1]))
        await interaction.response.send_message(
                f"The {channel} channel has been updated.", ephemeral=True
                )
        for team in teams:
            role = (int(team))
            role = interaction.guild.get_role(role)
            await channel.set_permissions(role, read_messages=True)
        


    @nextcord.ui.button(
        #MEET_THE_ENEMY_CHANNEL
        label="Meet The Enemy",
        style=nextcord.ButtonStyle.green,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.MEET_THE_ENEMY_CHANNEL),
    )
    async def meet_the_enemy(self, button, interaction: Interaction):
        await self.on_click(button, interaction)
        
    @nextcord.ui.button(
        #MEET_THE_ENEMY_CHANNEL
        label="Gameday",
        style=nextcord.ButtonStyle.green,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.GAMEDAY_CHANNEL),
    )
    async def gameday(self, button, interaction: Interaction):
        await self.on_click(button, interaction)
        
    @nextcord.ui.button(
        #MEET_THE_ENEMY_CHANNEL
        label="Offseason",
        style=nextcord.ButtonStyle.green,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.OFFSEASON_CHANNEL),
    )
    async def offseason(self, button, interaction: Interaction):
        await self.on_click(button, interaction)
        
    @nextcord.ui.button(
        #MEET_THE_ENEMY_CHANNEL
        label="NFL Draft",
        style=nextcord.ButtonStyle.green,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.NFL_DRAFT_CHANNEL),
    )
    async def nfl_draft(self, button, interaction: Interaction):
        await self.on_click(button, interaction)