import random
import csv

import config

import nextcord
from nextcord import Embed, Color, Interaction
from nextcord.ext import commands, tasks

class PurgeBot(commands.Cog):
    def __init__(self, client):
        self.client = client
        self.send_onready_message.start()

    # Send notification to log channel bot is loaded
    @tasks.loop(count=1)
    async def send_onready_message(self):
        channel = self.client.get_channel(int(config.BRONCO_BOT_CHANNEL))
        await channel.send('PurgeBot loaded')

    # wait for the client before starting the task
    @send_onready_message.before_loop  
    async def before_send(self):
        await self.client.wait_until_ready()
        return

    @send_onready_message.after_loop
    async def before_send(self):
        self.send_onready_message.close()
        return
    
    @commands.command()
    @commands.is_owner()
    async def purge_norole(self, ctx: commands.Context):
        """Kicks those without a role"""
        channel = self.client.get_channel(int(config.BRONCO_BOT_CHANNEL))
        guild = ctx.guild
        members = await guild.fetch_members().flatten()
        for member in members:
            roles = member.roles
            if len(roles) <= 1:
                await member.kick(reason= "Did not agree to rules")
                await channel.send(f'{member.name} kicked for not agreeing to rules')
                
    @commands.command()
    @commands.is_owner()
    async def purge_undrafted(self, ctx: commands.Context):
        """Kicks those without a team"""
        channel = self.client.get_channel(int(config.BRONCO_BOT_CHANNEL))
        guild = ctx.guild
        members = await guild.fetch_members().flatten()
        for member in members:
            roles = member.roles
            new_role_list = [role.id for role in roles]
            if int(config.UNDRAFTED_ROLE_ID) in new_role_list:
                await member.kick(reason= "Did not choose a team")
                await channel.send(f'{member.name} is undrafted and has been kicked')

def setup(client):
    client.add_cog(PurgeBot(client))