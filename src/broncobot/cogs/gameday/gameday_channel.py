import config

import nextcord
from nextcord.ext import commands, tasks


class GamedayChannelBot(commands.Cog):
    def __init__(self, client):
        self.client = client
        self.send_onready_message.start()


    # Send notification to log channel bot is loaded
    @tasks.loop(count=1)
    async def send_onready_message(self):
        # Go to https://sports.core.api.espn.com/v2/sports/football/leagues/nfl/seasons/2022/teams/7/events and save json to local folder
        channel = self.client.get_channel(int(config.BRONCO_BOT_CHANNEL))
        await channel.send('gameday channel loaded')

    # wait for the client before starting the task
    @send_onready_message.before_loop  
    async def before_send(self):
        await self.client.wait_until_ready()
        return

    @send_onready_message.after_loop
    async def before_send(self):
        self.send_onready_message.close()
        return