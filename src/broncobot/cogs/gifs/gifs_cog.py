import random
import csv

import config

import nextcord
from nextcord import Embed, Color, Interaction
from nextcord.ext import commands, tasks


def open_csv(filename):
    csv_list = []
    with open(filename, 'r') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        #next(readCSV, None) #if you want to skip header row
        for row in readCSV:
            csv_list.append(row[0])
    return csv_list


def rand_funct(chance_percent):
    rand_list = []
    for i in range(0,chance_percent):
        x = random.randint(1,100)
        rand_list.append(x)
    return rand_list


class gifBot(commands.Cog):
    def __init__(self, client):
        self.client = client
        self.send_onready_message.start()
        self.gooch_csv = open_csv('./cogs/gifs/assets/gooch.csv')
        self.ftr_csv = open_csv('./cogs/gifs/assets/ftr.csv')
        self.ftq_csv = open_csv('./cogs/gifs/assets/ftq.csv')
        self.ftc_csv = open_csv('./cogs/gifs/assets/ftc.csv')

    # Send notification to log channel bot is loaded
    @tasks.loop(count=1)
    async def send_onready_message(self):
        channel = self.client.get_channel(int(config.BRONCO_BOT_CHANNEL))
        await channel.send('gifs loaded')

    # wait for the client before starting the task
    @send_onready_message.before_loop  
    async def before_send(self):
        await self.client.wait_until_ready()
        return

    @send_onready_message.after_loop
    async def before_send(self):
        self.send_onready_message.close()
        return

    # Gif Bot Commands
    # TODO Convert to Slash Commands
    @commands.cooldown(1,5, commands.BucketType.user) 
    @commands.command()
    async def gooch(self, ctx: commands.Context):
        await ctx.send(random.choice(self.gooch_csv))

    @commands.cooldown(1,5, commands.BucketType.user) 
    @commands.command()
    async def ftr(self, ctx: commands.Context):
        await ctx.send(random.choice(self.ftr_csv))

    @commands.cooldown(1,5, commands.BucketType.user) 
    @commands.command()
    async def ftq(self, ctx: commands.Context):
        await ctx.send(random.choice(self.ftq_csv))

    @commands.cooldown(1,5, commands.BucketType.user) 
    @commands.command()
    async def ftc(self, ctx: commands.Context):
        await ctx.send(random.choice(self.ftc_csv))

    # Send message to gif spammer
    @commands.Cog.listener()
    async def on_command_error(self, ctx, error):
        author = ctx.message.author.id
        if isinstance(error, commands.CommandOnCooldown):
            em = Embed(title=f'You are spamming', 
                    description=f'Try again in {error.retry_after:.2f} seconds', 
                    color=Color.red())
            await ctx.message.author.send(embed=em)


def setup(client):
    client.add_cog(gifBot(client))
