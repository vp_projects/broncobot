import config

import nextcord
from nextcord.ext import commands, tasks


class memberJoinBot(commands.Cog):
    def __init__(self, client):
        self.client = client
        self.send_onready_message.start()

    #Send notification to log channel bot is loaded
    @tasks.loop(count=1)
    async def send_onready_message(self):
        channel = self.client.get_channel(int(config.BRONCO_BOT_CHANNEL))
        await channel.send('MemberJoin Loaded')

    # wait for the client before starting the task
    @send_onready_message.before_loop  
    async def before_send(self):
        await self.client.wait_until_ready()
        return

    @send_onready_message.after_loop
    async def before_send(self):
        self.send_onready_message.close()
        return
    
    @commands.Cog.listener()
    async def on_member_update(self, before, after):
        old_roles = before.roles
        new_roles = after.roles
        channel = self.client.get_channel(int(config.WELCOME_CHANNEL))
        if len(old_roles) < len(new_roles):
            newRole = next(role for role in new_roles if role not in old_roles)
            if newRole.name != 'Undrafted':
                new_role_list = [role.name for role in new_roles]
                if len(new_roles) == 3 and 'Undrafted' in new_role_list:
                    if int(newRole.id) == int(config.HORSEBRO_ROLE_ID):
                        await channel.send(f'Horse Bro')
                    elif int(newRole.id) == int(config.RAIDER_ROLE_ID):
                        await channel.send(f'From the south blows a wretched stench welcome {after.name} who is a Raider fan')
                    elif int(newRole.id) == int(config.CHIEFS_ROLE_ID):
                        await channel.send(f'Are you ready from some cringy Tik Toks for here comes {after.name} who is a Chief\'s fan')
                    elif int(newRole.id) == int(config.CHARGERS_ROLE_ID):
                        await channel.send(f'Please welcome {after.name}, this poor soul doesn\'t even have a home stadium for they are a Charger\'s fan')
                    else:
                        await channel.send(f'Please welcome {after.name}, who supports the {newRole.name}')


        
def setup(client):
    client.add_cog(memberJoinBot(client))