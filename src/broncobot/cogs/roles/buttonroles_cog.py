import nextcord
from nextcord import Interaction, Embed, Color
from nextcord.ext import commands, tasks
import config
from .role_view import RoleView
from .role_view2 import RoleView2
from .confirm_view import ConfirmView


class ButtonRolesBot(commands.Cog, name="Button Roles"):
    """Give and remove roles based on button presses"""

    def __init__(self, client):
        self.client = client
        self.send_onready_message.start()
        
    #Send notification to log channel bot is loaded
    @tasks.loop(count=1)
    async def send_onready_message(self):
        channel = self.client.get_channel(int(config.BRONCO_BOT_CHANNEL))
        await channel.send('ButtonRoles loaded')

    # wait for the client before starting the task
    @send_onready_message.before_loop  
    async def before_send(self):
        await self.client.wait_until_ready()
        return

    @send_onready_message.after_loop
    async def before_send(self):
        self.send_onready_message.close()
        return

    @commands.Cog.listener()
    async def on_ready(self):
        """When the bot is ready, load the role view"""
        self.client.add_view(RoleView())
        self.client.add_view(RoleView2())
        self.client.add_view(ConfirmView())
        print("Button view added")

    @commands.command()
    @commands.is_owner()
    async def roles(self, ctx: commands.Context):
        """Starts a role view"""
        await ctx.send('Click a button to add or remove a role', view=RoleView())
        await ctx.send(view=RoleView2())
        
    @commands.command()
    @commands.is_owner()
    async def roles2(self, ctx: commands.Context):
        """Starts a role view"""
        await ctx.send(view=RoleView2())
        
    @commands.command()
    @commands.is_owner()
    async def confirm(self, ctx: commands.Context):
        """Starts a confirm view"""
        rules_channel = self.client.get_channel(int(config.RULE_CHANNEL))
        message = await rules_channel.fetch_message(int(config.RULES_MESSAGE_ID))
        await message.edit(view=ConfirmView())
        


# setup functions for bot
def setup(client):
    client.add_cog(ButtonRolesBot(client))
    