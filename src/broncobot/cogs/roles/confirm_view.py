from utils.utils import custom_id
import nextcord
from nextcord import Interaction
import config

VIEW_NAME = "ConfirmView"


class ConfirmView(nextcord.ui.View):
    def __init__(self):
        super().__init__(timeout=None)
        
    async def confirm_click(self,
                            button: nextcord.ui.Button,
                            interaction: nextcord.Interaction):

        # get role from the role id
        try:
            role = interaction.guild.get_role(int(config.UNDRAFTED_ROLE_ID))
            assert isinstance(role, nextcord.Role)
            if role in interaction.user.roles:
                await interaction.response.send_message(
                f"You have already confirmed", ephemeral=True)
            else:
                # Assign Undrafted Role once confirm button is clicked.
                await interaction.user.add_roles(role)
                # send confirmation message
                await interaction.response.send_message(
                    f"Thanks for confirming, please select a team",
                    ephemeral=True
                    )
        except:
            await interaction.response.send_message(
                f"There was an error please contact a mod for assistance",
                ephemeral=True
                    )


    @nextcord.ui.button(
        label="Confirm",
        emoji="👍",
        style=nextcord.ButtonStyle.green,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.UNDRAFTED_ROLE_ID),
    )
    async def confirm_button(self, button, interaction):
        await self.confirm_click(button, interaction)
        #await interaction.response.send_message("Confirm")