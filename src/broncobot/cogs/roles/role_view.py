from utils.utils import custom_id
import nextcord
from nextcord import Interaction
import config

VIEW_NAME = "RoleView"


class RoleView(nextcord.ui.View):
    def __init__(self):
        super().__init__(timeout=None)
        
    async def handle_click(
        self, button: nextcord.ui.Button, interaction: nextcord.Interaction
    ):
        user_roles = interaction.user.roles
        # get role from the role id
        role = interaction.guild.get_role(int(button.custom_id.split(":")[-1]))
        assert isinstance(role, nextcord.Role)
        
        if len(user_roles) <= 1:
            await interaction.response.send_message(
                f"Please confirm the rules before choosing a team.", ephemeral=True
            )
        # if member has role remove it, check to see if any other roles are present    
        elif role in user_roles:
            if len(user_roles) >= 3:
                await interaction.user.remove_roles(role)
                await interaction.response.send_message(
                f"The {button.label} role has been removed.", ephemeral=True
                )
                
            else:
                # Apply Undrafted role
                undrafted_role = interaction.guild.get_role(int(config.UNDRAFTED_ROLE_ID))
                assert isinstance(undrafted_role, nextcord.Role)
                await interaction.user.add_roles(undrafted_role)
                # Remove requested role from member
                await interaction.user.remove_roles(role)
                # send confirmation message
                await interaction.response.send_message(
                f"The {button.label} role has been removed. Please choose a new team.", ephemeral=True
            )
        # if member does not have role add it
        else:
            await interaction.user.add_roles(role)
            # Remove Undrafted Role
            undrafted_role = interaction.guild.get_role(int(config.UNDRAFTED_ROLE_ID))
            assert isinstance(undrafted_role, nextcord.Role)
            await interaction.user.remove_roles(undrafted_role)
            # send confirmation message
            await interaction.response.send_message(
                f"You have been given the {button.label} role", ephemeral=True
            )

    @nextcord.ui.button(
        label="Broncos",
        style=nextcord.ButtonStyle.green,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.HORSEBRO_ROLE_ID),
    )
    async def horsebro_button(self, button, interaction):
        await self.handle_click(button, interaction)
        
    @nextcord.ui.button(
        label="Cardinals",
        style=nextcord.ButtonStyle.blurple,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.CARDINALS_ROLE_ID),
    )
    async def cardinal_button(self, button, interaction):
        await self.handle_click(button, interaction)
        
    @nextcord.ui.button(
        label="Falcons",
        style=nextcord.ButtonStyle.blurple,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.FALCONS_ROLE_ID),
    )
    async def falcons_button(self, button, interaction):
        await self.handle_click(button, interaction)
        
    @nextcord.ui.button(
        label="Ravens",
        style=nextcord.ButtonStyle.blurple,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.RAVENS_ROLE_ID),
    )
    async def raven_button(self, button, interaction):
        await self.handle_click(button, interaction)
        
    @nextcord.ui.button(
        label="Bills",
        style=nextcord.ButtonStyle.blurple,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.BILLS_ROLE_ID),
    )
    async def bills_button(self, button, interaction):
        await self.handle_click(button, interaction)
        
    @nextcord.ui.button(
        label="Panthers",
        style=nextcord.ButtonStyle.blurple,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.PANTHERS_ROLE_ID),
    )
    async def panther_button(self, button, interaction):
        await self.handle_click(button, interaction)
        
    @nextcord.ui.button(
        label="Bears",
        style=nextcord.ButtonStyle.blurple,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.BEARS_ROLE_ID),
    )
    async def bear_button(self, button, interaction):
        await self.handle_click(button, interaction)
        
    @nextcord.ui.button(
        label="Bengals",
        style=nextcord.ButtonStyle.blurple,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.BENGALS_ROLE_ID),
    )
    async def bengals_button(self, button, interaction):
        await self.handle_click(button, interaction)
        
    @nextcord.ui.button(
        label="Browns",
        style=nextcord.ButtonStyle.blurple,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.BROWNS_ROLE_ID),
    )
    async def browns_button(self, button, interaction):
        await self.handle_click(button, interaction)
        
    @nextcord.ui.button(
        label="Cowboys",
        style=nextcord.ButtonStyle.blurple,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.COWBOYS_ROLE_ID),
    )
    async def cowboys_button(self, button, interaction):
        await self.handle_click(button, interaction)
        
    @nextcord.ui.button(
        label="Lions",
        style=nextcord.ButtonStyle.blurple,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.LIONS_ROLE_ID),
    )
    async def lions_button(self, button, interaction):
        await self.handle_click(button, interaction)
        
    @nextcord.ui.button(
        label="Packers",
        style=nextcord.ButtonStyle.blurple,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.PACKERS_ROLE_ID),
    )
    async def packers_button(self, button, interaction):
        await self.handle_click(button, interaction)
        
    @nextcord.ui.button(
        label="Texans",
        style=nextcord.ButtonStyle.blurple,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.TEXANS_ROLE_ID),
    )
    async def TEXANS_button(self, button, interaction):
        await self.handle_click(button, interaction)
        
    @nextcord.ui.button(
        label="Colts",
        style=nextcord.ButtonStyle.blurple,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.COLTS_ROLE_ID),
    )
    async def colts_button(self, button, interaction):
        await self.handle_click(button, interaction)
        
    @nextcord.ui.button(
        label="Jags",
        style=nextcord.ButtonStyle.blurple,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.JAGS_ROLE_ID),
    )
    async def jags_button(self, button, interaction):
        await self.handle_click(button, interaction)
        
    @nextcord.ui.button(
        label="Rams",
        style=nextcord.ButtonStyle.blurple,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.RAMS_ROLE_ID),
    )
    async def rams_button(self, button, interaction):
        await self.handle_click(button, interaction)
        
    @nextcord.ui.button(
        label="Dolphins",
        style=nextcord.ButtonStyle.blurple,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.DOLPHINS_ROLE_ID),
    )
    async def dolphins_button(self, button, interaction):
        await self.handle_click(button, interaction)
        
    @nextcord.ui.button(
        label="Vikings",
        style=nextcord.ButtonStyle.blurple,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.VIKINGS_ROLE_ID),
    )
    async def vikings_button(self, button, interaction):
        await self.handle_click(button, interaction)
        
    @nextcord.ui.button(
        label="Patriots",
        style=nextcord.ButtonStyle.blurple,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.PATRIOTS_ROLE_ID),
    )
    async def pats_button(self, button, interaction):
        await self.handle_click(button, interaction)
        
    @nextcord.ui.button(
        label="Saints",
        style=nextcord.ButtonStyle.blurple,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.SAINTS_ROLE_ID),
    )
    async def saints_button(self, button, interaction):
        await self.handle_click(button, interaction)