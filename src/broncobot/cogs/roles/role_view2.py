from utils.utils import custom_id
import nextcord
from nextcord import Interaction
import config

VIEW_NAME = "RoleView2"


class RoleView2(nextcord.ui.View):
    def __init__(self):
        super().__init__(timeout=None)
        
    async def handle_click(
        self, button: nextcord.ui.Button, interaction: nextcord.Interaction
    ):
        user_roles = interaction.user.roles
        # get role from the role id
        role = interaction.guild.get_role(int(button.custom_id.split(":")[-1]))
        assert isinstance(role, nextcord.Role)
        
        if len(user_roles) <= 1:
            await interaction.response.send_message(
                f"Please confirm the rules before choosing a team.", ephemeral=True
            )
        # if member has role remove it, check to see if any other roles are present    
        elif role in user_roles:
            if len(user_roles) >= 3:
                await interaction.user.remove_roles(role)
                await interaction.response.send_message(
                f"The {button.label} role has been removed.", ephemeral=True
                )
                
            else:
                # Apply Undrafted role
                undrafted_role = interaction.guild.get_role(int(config.UNDRAFTED_ROLE_ID))
                assert isinstance(undrafted_role, nextcord.Role)
                await interaction.user.add_roles(undrafted_role)
                # Remove requested role from member
                await interaction.user.remove_roles(role)
                # send confirmation message
                await interaction.response.send_message(
                f"The {button.label} role has been removed. Please choose a new team.", ephemeral=True
            )
        # if member does not have role add it
        else:
            await interaction.user.add_roles(role)
            # Remove Undrafted Role
            undrafted_role = interaction.guild.get_role(int(config.UNDRAFTED_ROLE_ID))
            assert isinstance(undrafted_role, nextcord.Role)
            await interaction.user.remove_roles(undrafted_role)
            # send confirmation message
            await interaction.response.send_message(
                f"You have been given the {button.label} role", ephemeral=True
            )
            

    @nextcord.ui.button(
        label="Giants",
        style=nextcord.ButtonStyle.blurple,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.GIANTS_ROLE_ID),
    )
    async def giants_button(self, button, interaction):
        await self.handle_click(button, interaction)
        
    @nextcord.ui.button(
        label="Jets",
        style=nextcord.ButtonStyle.blurple,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.JETS_ROLE_ID),
    )
    async def jets_button(self, button, interaction):
        await self.handle_click(button, interaction)
        
    @nextcord.ui.button(
        label="Eagles",
        style=nextcord.ButtonStyle.blurple,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.EAGLES_ROLE_ID),
    )
    async def eagles_button(self, button, interaction):
        await self.handle_click(button, interaction)
        
    @nextcord.ui.button(
        label="Steelers",
        style=nextcord.ButtonStyle.blurple,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.STEELERS_ROLE_ID),
    )
    async def steelers_button(self, button, interaction):
        await self.handle_click(button, interaction)
        
    @nextcord.ui.button(
        label="49ers",
        style=nextcord.ButtonStyle.blurple,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.SF49_ROLE_ID),
    )
    async def sf49_button(self, button, interaction):
        await self.handle_click(button, interaction)
        
    @nextcord.ui.button(
        label="Seahawks",
        style=nextcord.ButtonStyle.blurple,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.SEAHAWKS_ROLE_ID),
    )
    async def seahawks_button(self, button, interaction):
        await self.handle_click(button, interaction)
        
    @nextcord.ui.button(
        label="Buccaneers",
        style=nextcord.ButtonStyle.blurple,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.BUCCANEERS_ROLE_ID),
    )
    async def buccaneers_button(self, button, interaction):
        await self.handle_click(button, interaction)
        
    @nextcord.ui.button(
        label="Titans",
        style=nextcord.ButtonStyle.blurple,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.TITANS_ROLE_ID),
    )
    async def titans_button(self, button, interaction):
        await self.handle_click(button, interaction)
        
    @nextcord.ui.button(
        label="Commanders",
        style=nextcord.ButtonStyle.blurple,
        # set custom id to be the bot name : the class name : the role id
        custom_id=custom_id(VIEW_NAME, config.COMMANDERS_ROLE_ID),
    )
    async def commanders_button(self, button, interaction):
        await self.handle_click(button, interaction)
        
    @nextcord.ui.button(
        label="Chargers",
        style=nextcord.ButtonStyle.grey,
        custom_id=custom_id(VIEW_NAME, config.CHARGERS_ROLE_ID),
    )
    async def chargers_button(self, button, interaction):
        await self.handle_click(button, interaction)
        
    @nextcord.ui.button(
        label="Chiefs",
        style=nextcord.ButtonStyle.grey,
        custom_id=custom_id(VIEW_NAME, config.CHIEFS_ROLE_ID),
    )
    async def chiefs_button(self, button, interaction):
        await self.handle_click(button, interaction)
        
    @nextcord.ui.button(
        label="Raiders",
        style=nextcord.ButtonStyle.grey,
        custom_id=custom_id(VIEW_NAME, config.RAIDER_ROLE_ID),
    )
    async def raider_button(self, button, interaction):
        await self.handle_click(button, interaction)
