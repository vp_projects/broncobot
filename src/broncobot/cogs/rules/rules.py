import config

descript = 'Welcome to the **Denver Broncos** Discord Server'

mod_list = f'<@{config.SPRUK_ID}> \n <@{config.QEL_ID}> \n <@{config.RUBY_ID}> \n <@{config.FLAAFY_ID}> \n <@{config.SOFA_ID}> \n \
        Or Tag <@&{config.ELWAYS_ELITE}>'


rule_1 = '1. Fuck the Raiders'

rule_1a = '1a. Fuck Tom Brady'

rule_2 = '2. You can rough-house, but ultimately treat others with respect'

rule_3 = '3. Racism, homophobia, etc will not be tolerated.'

rule_4 = '4. Avoid controversial topics such as politics or religion'

rule_5 = '5. No self promotion or advertising'

rule_6 = '6. Absolutely no NSFW content in any of our channels..'

rule_7 = '7. We are no longer allowing direct links to Streams. If someone asks \
        for a stream please DM them the link. Any streams posted in chat will \
        be removed by the mods.'
        
rule_8a = '8. Remember you represent this server when you interact with others. \
        Show respect. Going into other teams servers and spamming, shit \
        talking, etc or generally breaking any of their rules or our rules, \
        will not be tolerated. We have ways to see if you are causing trouble \
        in other servers. This will be an instant ban if we hear you are \
        causing issues in other servers. '
        
rule_8 = '8. Remember you represent this server when you interact with others. \
        Going into other teams servers and spamming, shit \
        talking, etc or generally breaking any of their rules or our rules, \
        will not be tolerated.'
    
rule_9 = "Please respect and follow Discord's Terms of Service \n \
                - Anything that breaks these terms will result in a ban and a report to Discord. \n \
                - Discord Guidlines: https://dis.gd/guidelines \n \
                - Discord TOS: https://discord.com/terms"

rule_list_1 = f'{rule_1} \n \
        {rule_1a} \n \
        {rule_2} \n \
        {rule_3} \n \
        {rule_4} \n \
        {rule_5} \n \
        {rule_6} \n \
        {rule_7} \n \
        {rule_8}'

rule_list_2 = f'{rule_9}'