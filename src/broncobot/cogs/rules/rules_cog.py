import config

import nextcord
from nextcord import Embed, Color, Interaction
from nextcord.ext import commands, tasks

from .rules import rule_list_1, rule_list_2, descript, mod_list



class RuleBot(commands.Cog):
    def __init__(self, client):
        self.client = client
        self.send_onready_message.start()

    #Send notification to log channel bot is loaded
    @tasks.loop(count=1)
    async def send_onready_message(self):
        channel = self.client.get_channel(int(config.BRONCO_BOT_CHANNEL))
        await channel.send('Rules Loaded')

    # wait for the client before starting the task
    @send_onready_message.before_loop  
    async def before_send(self):
        await self.client.wait_until_ready()
        return

    @send_onready_message.after_loop
    async def before_send(self):
        self.send_onready_message.close()
        return
    
    @commands.command()
    async def post_rules(self, ctx):
        em = Embed(title=f'🚨 Denver Broncos Server Rules', 
                    description= descript, 
                    color=nextcord.Color.from_rgb(251, 79, 20))
        
        em.add_field(name='Rules', value= rule_list_1, inline=False)
        em.add_field(name="Discord Terms of Service", value=rule_list_2, inline=False)
        em.add_field(name="If you need a Mod", value=mod_list, inline=False)
        em.add_field(name=f"{config.DISCORD_EMOJI} Server Invite", value=config.SERVER_INVITE, inline=False)
        em.add_field(name=f"Confirmation", value='Once you have read these rules, click confirm and choose a team role to gain access to the rest of the server.', inline=False)
        await ctx.send(embed=em)
        
        
def setup(client):
    client.add_cog(RuleBot(client))
