import os
from dotenv.main import load_dotenv

load_dotenv()

# Annotate Dev or Pro Enviroment
ENV_TYPE = os.getenv("ENVTYPE")

# Bot Info
BOT_TOKEN = os.getenv("BOT_TOKEN")
BOT_NAME = os.getenv("BOT_NAME")
PREFIX = os.getenv("PREFIX")

TEST_SERVER_ID = os.getenv("TEST_SERVER_ID")

SERVER_INVITE = os.getenv("SERVER_INVITE")
DISCORD_EMOJI = os.getenv("DISCORD_EMOJI")

# Discord Channels
BRONCO_BOT_CHANNEL = os.getenv("BRONCO_BOT_CHANNEL")
GENERAL_CHANNEL = os.getenv("GENERAL_CHANNEL")
RULE_CHANNEL = os.getenv("RULE_CHANNEL")
WELCOME_CHANNEL = os.getenv("WELCOME_CHANNEL")
RULE_CHANNEL = os.getenv("RULE_CHANNEL")
MEET_THE_ENEMY_CHANNEL = os.getenv("MEET_THE_ENEMY_CHANNEL")
GAMEDAY_CHANNEL = os.getenv("GAMEDAY_CHANNEL")
OFFSEASON_CHANNEL = os.getenv("OFFSEASON_CHANNEL")
NFL_DRAFT_CHANNEL = os.getenv("NFL_DRAFT_CHANNEL")



# Broncos Mods
SPRUK_ID = os.getenv("SPRUK_ID")
QEL_ID = os.getenv("QEL_ID")
RUBY_ID = os.getenv("RUBY_ID")
FLAAFY_ID = os.getenv("FLAAFY_ID")
SOFA_ID = os.getenv("SOFA_ID")

# Broncos Mod ID
ELWAYS_ELITE = os.getenv("ELWAYS_ELITE")

# Discord Message IDs
RULES_MESSAGE_ID = int(os.getenv("RULES_MESSAGE_ID"))

# Discord ROLEs
UNDRAFTED_ROLE_ID = os.getenv("UNDRAFTED_ROLE_ID")
HORSEBRO_ROLE_ID = os.getenv("HORSEBRO_ROLE_ID")
RAIDER_ROLE_ID = os.getenv("RAIDER_ROLE_ID")
JETS_ROLE_ID = os.getenv("JETS_ROLE_ID")
PACKERS_ROLE_ID = os.getenv("PACKERS_ROLE_ID")
JAGS_ROLE_ID = os.getenv("JAGS_ROLE_ID")
SAINTS_ROLE_ID = os.getenv("SAINTS_ROLE_ID")
SF49_ROLE_ID = os.getenv("49_ROLE_ID")
VIKINGS_ROLE_ID = os.getenv("VIKINGS_ROLE_ID")
SEAHAWKS_ROLE_ID = os.getenv("SEAHAWKS_ROLE_ID")
GIANTS_ROLE_ID = os.getenv("GIANTS_ROLE_ID")
BROWNS_ROLE_ID = os.getenv("BROWNS_ROLE_ID")
STEELERS_ROLE_ID = os.getenv("STEELERS_ROLE_ID")
CARDINALS_ROLE_ID = os.getenv("CARDINALS_ROLE_ID")
FALCONS_ROLE_ID = os.getenv("FALCONS_ROLE_ID")
PANTHERS_ROLE_ID = os.getenv("PANTHERS_ROLE_ID")
BEARS_ROLE_ID = os.getenv("BEARS_ROLE_ID")
COWBOYS_ROLE_ID = os.getenv("COWBOYS_ROLE_ID")
LIONS_ROLE_ID = os.getenv("LIONS_ROLE_ID")
RAMS_ROLE_ID = os.getenv("RAMS_ROLE_ID")
EAGLES_ROLE_ID = os.getenv("EAGLES_ROLE_ID")
BUCCANEERS_ROLE_ID = os.getenv("BUCCANEERS_ROLE_ID")
COMMANDERS_ROLE_ID = os.getenv("COMMANDERS_ROLE_ID")
RAVENS_ROLE_ID = os.getenv("RAVENS_ROLE_ID")
BILLS_ROLE_ID = os.getenv("BILLS_ROLE_ID")
BENGALS_ROLE_ID = os.getenv("BENGALS_ROLE_ID")
TEXANS_ROLE_ID = os.getenv("TEXANS_ROLE_ID")
COLTS_ROLE_ID = os.getenv("COLTS_ROLE_ID")
CHIEFS_ROLE_ID = os.getenv("CHIEFS_ROLE_ID")
CHARGERS_ROLE_ID = os.getenv("CHARGERS_ROLE_ID")
DOLPHINS_ROLE_ID = os.getenv("DOLPHINS_ROLE_ID")
PATRIOTS_ROLE_ID = os.getenv("PATRIOTS_ROLE_ID")
TITANS_ROLE_ID = os.getenv("TITANS_ROLE_ID")