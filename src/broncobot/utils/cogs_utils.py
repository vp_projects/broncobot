import os

class BotCogs():
    def __init__(self):
        self.all_cogs_full = None
        self.cog_dict = None
        self.loaded_cogs = []
        
        
        self.get_all_cogs()
        self.create_cog_dict(self.all_cogs_full)

    
    def get_all_cogs(self):
        all_cogs_list = []
        for folder in os.listdir("cogs"):
            if os.path.exists(os.path.join("cogs", folder)):
                for filename in os.listdir(f'./cogs/{folder}'):
                    if filename.endswith('cog.py'):
                        if os.path.exists(os.path.join("cogs",
                                                       folder,
                                                       filename)):
                            all_cogs_list.append(f"cogs.{folder}.{filename[:-3]}")
        self.all_cogs_full = all_cogs_list
      
        
    def create_cog_dict(self, cog_path):
        cog_dict = {}
        for cog in cog_path:
            cog_path_list = cog.split('.')
            cog_dict[cog_path_list[-1][:-4]] = cog
        self.cog_dict = cog_dict

    
    def get_keys(self, val):
        for key, value in self.cog_dict.items():
            if val == value:
                return key
        return "key not found"