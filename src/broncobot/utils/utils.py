import re
import nextcord
import config

teams = [config.HORSEBRO_ROLE_ID, config.RAIDER_ROLE_ID, config.JETS_ROLE_ID, 
         config.PACKERS_ROLE_ID, config.JAGS_ROLE_ID, config.SAINTS_ROLE_ID, 
         config.SF49_ROLE_ID, config.VIKINGS_ROLE_ID, config.SEAHAWKS_ROLE_ID,
         config.GIANTS_ROLE_ID, config.BROWNS_ROLE_ID, config.STEELERS_ROLE_ID,
         config.CARDINALS_ROLE_ID, config.FALCONS_ROLE_ID, config.PANTHERS_ROLE_ID,
         config.BEARS_ROLE_ID, config.COWBOYS_ROLE_ID, config.LIONS_ROLE_ID,
         config.RAMS_ROLE_ID, config.EAGLES_ROLE_ID, config.BUCCANEERS_ROLE_ID,
         config.COMMANDERS_ROLE_ID, config.RAVENS_ROLE_ID, config.BILLS_ROLE_ID,
         config.BENGALS_ROLE_ID, config.TEXANS_ROLE_ID, config.COLTS_ROLE_ID,
         config.CHIEFS_ROLE_ID, config.CHARGERS_ROLE_ID, config.DOLPHINS_ROLE_ID,
         config.PATRIOTS_ROLE_ID, config.TITANS_ROLE_ID]

def custom_id(view: str, id: int) -> str:
    """create a custom id from the bot name : the view : the identifier"""
    return f"{config.BOT_NAME}:{view}:{id}"
