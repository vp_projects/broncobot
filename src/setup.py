from setuptools import setup

setup(
    name='broncobot',
    version='0.0.1',
    description='This is a simple Discord bot for the Denver Broncos Server',
    author_email='tyson@descriptdata.com',
    url='www.descriptdata.com',
    install_requires=[
        'nextcord >= 2.0.0a9, < 3.0.0',
        'python-dotenv >= 0.19.2, <1.0.0',
        'requests >= 2.27.1, <3.0.0'
    ]
)